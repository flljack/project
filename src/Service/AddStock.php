<?php

namespace App\Service;

use App\Entity\BonusAction;
use App\Entity\Cafe;
use Doctrine\ORM\EntityManagerInterface;

class AddStock {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AddStock constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $content
     * @return bool
     */
    public function add(array $content)
    {
        if ($this->validateContent($content)) {
            $cafeId = (int) $content['cafeId'];
            $stockTitle = (string) $content['title'];
            $stockDescription = (string) $content['description'];
            $stockImageUrl = (string) $content['imageUrl'];
            $cafeEntity = $this->entityManager->getRepository(Cafe::class)->find($cafeId);
            if (!empty($cafeEntity)) {
                $stockEntity = new  BonusAction();
                $stockEntity->setTitle($stockTitle)
                    ->setDescription($stockDescription)
                    ->setImage($stockImageUrl)
                    ->setCafe($cafeEntity)
                ;
                $this->entityManager->persist($stockEntity);
                $this->entityManager->flush();
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $content
     * @return bool
     */
    private function validateContent(array $content)
    {
        if (empty($content['cafeId']) || empty($content['title']) || empty($content['description']) || empty($content['imageUrl'])) {
            return false;
        }
        return true;
    }
}