<?php

namespace App\Service\Grabbers\Core;

use Symfony\Component\HttpFoundation\Response;
use App\Entity\BonusAction;
use App\Entity\Cafe;
use Doctrine\ORM\EntityManager;

class Mapping
{
    /**
     * @var Cafe
     */
    private $cafe;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @param EntityManager $entityManager */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     */
    public function setCafeById(int $id)
    {
        $this->cafe = $this->entityManager->getRepository(Cafe::class)->findOneBy(['id' => $id]);
    }

    public function mappingAndSaveBonusAction(array $data)
    {
        $bonusAction = new BonusAction();
        $bonusAction->setTitle($data['title']);
        $bonusAction->setDescription($data['description']);
        $bonusAction->setImage($data['image']);
        if (!empty($data['url'])) {
            $bonusAction->setUrl($data['url']);
        }
        $bonusAction->setCafe($this->cafe);
        $this->entityManager->persist($bonusAction);
        $this->entityManager->flush();
    }
}