<?php

namespace App\Service\Grabbers\Core;

class Spider
{
    const DEFAULT_USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36';

    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;

    /**
     * @var \GuzzleHttp\Cookie\CookieJar
     */
    private $cookie;

    /**
     * @var string
     */
    private $userAgent;

    /**
     * @param array $otherHeaders
     * Spider constructor.
     */
    public function __construct($otherHeaders = [])
    {
        $this->userAgent = self::DEFAULT_USER_AGENT;
        $this->httpClient = $this->createHttpClient($otherHeaders);
        $this->cookie = new \GuzzleHttp\Cookie\CookieJar();
        echo('Spider core construct' . PHP_EOL);
    }

    /**
     * @return \GuzzleHttp\Client
     */
    protected function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @return \GuzzleHttp\Cookie\CookieJar
     */
    protected function getCookie()
    {
        return $this->cookie;
    }

    /**
     * @return string
     */
    protected function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     */
    protected function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    /**
     * @param array $otherHeaders
     * @return \GuzzleHttp\Client
     */
    private function createHttpClient($otherHeaders = [])
    {
        $params = [
            'headers' => [
                'User-Agent' => $this->getUserAgent(),
                'Connection' => 'keep-alive',
                'Keep-alive' => '300',
                'Accept-Encoding' => 'gzip, deflate'
            ],
            'curl' => [
                CURLOPT_SSL_VERIFYPEER => false
            ]
        ];
        if (!empty($otherHeaders)) {
            $params['headers'] = array_merge($params['headers'], $otherHeaders);
        }

        return new \GuzzleHttp\Client($params);
    }

    /**
     * @param string $url
     * @return string
     */
    public function getPageContent($url) 
    {
        return $this->getHttpClient()
            ->get($url)
            ->getBody()
            ->getContents();
    }
}