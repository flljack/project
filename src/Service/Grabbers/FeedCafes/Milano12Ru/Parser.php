<?php

namespace App\Service\Grabbers\FeedCafes\Milano12Ru;

use App\Service\Grabbers\Core\Mapping;

class Parser
{
    /**
     * @var Mapping
     */
    private $mapping;

    /** @param EntityManager $entityManager */
    public function __construct($entityManager)
    {
        $this->mapping = new Mapping($entityManager);
        $this->mapping->setCafeById(2);
    }

    /**
     * @param string $pageContent
     * @return array
     */
    public function parseFullUrl($pageContent)
    {
        $bonusActionUrls = [];
        $document = \phpQuery::newDocument($pageContent);
        $bonusActionsUrlElements = $document->find('#actions div.link.text-center a');
        foreach ($bonusActionsUrlElements as $element) {
            $pq = pq($element);
            $bonusActionUrls[] = $pq->attr('href');
        }
        return $bonusActionUrls;
    }

    /**
     * @param string $pageContent
     * @param string $url
     */
    public function parseBonusAction($pageContent, $url)
    {
        $document = \phpQuery::newDocument($pageContent);
        $image = $document->find('.img img')->attr('src');
        $title =  $document->find('.name')->text();
        $description = $document->find('.desc')->text();
        if (!isset($image) || !isset($title) || !isset($description)) {
            return;
        }
        $data = ['image' => $image, 'title' => $title, 'description' => $description, 'url' => $url];
        $this->mapping->mappingAndSaveBonusAction($data);
    }
}