<?php

namespace App\Service\Grabbers\FeedCafes\Milano12Ru;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use App\Service\Grabbers\Core\Spider;

class Grabber
{

    const URL_SPIDER = 'http://milano12.ru/actions';
    const URL_BASE = 'http://milano12.ru';
    /** @var Spider */
    private $spider;

    /**
     * @var Parser
     */
    private $parser;
    /**
     * @param App\Service\Grabbers\Core\Spider $spider
     * @param Parser $parser
     */
    public function __construct(App\Service\Grabbers\Core\Spider $spider, Parser $parser)
    {
        $this->spider = $spider;
        $this->parser = $parser;
    }
    
    public function grab() 
    {
        $pageContent = $this->spider->getPageContent(self::URL_SPIDER);
        $bonusActionUrls = $this->parser->parseFullUrl($pageContent);
        foreach ($bonusActionUrls as $bonusActionUrl) {
            $pageContent = $this->spider->getPageContent(self::URL_BASE . $bonusActionUrl);
            $this->parser->parseBonusAction($pageContent, self::URL_BASE . $bonusActionUrl);
        }
    }

}