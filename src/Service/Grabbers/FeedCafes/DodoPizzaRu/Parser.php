<?php

namespace App\Service\Grabbers\FeedCafes\DodoPizzaRu;

use App\Service\Grabbers\Core\Mapping;

class Parser
{
    /**
     * @var Mapping
     */
    private $mapping;

    /** @param EntityManager $entityManager */
    public function __construct($entityManager)
    {
        $this->mapping = new Mapping($entityManager);
    }
    
    /**
     * @param string $pageContent
     */
    public function parse($pageContent) 
    {
        $this->mapping->setCafeById(1);
        $bonusActionsElements = json_decode($pageContent, true);
        foreach ($bonusActionsElements as $element) {
            $image = $element['bannerPath'];
            $title =  $element['title'];
            $description = $element['description'];
            if (!isset($image) || !isset($title) || !isset($description)) {
                continue;
            }
            $data = ['image' => $image, 'title' => $title, 'description' => $description];
            $this->mapping->mappingAndSaveBonusAction($data);
        }
    }
}