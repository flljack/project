<?php

namespace App\Service\Grabbers\FeedCafes\DodoPizzaRu;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use App\Service\Grabbers\Core\Spider;

class Grabber
{

    const URL_SPIDER = 'https://dodopizza.ru/api/bonus-actions?localityId=0000002b-0000-0000-0000-000000000000';

    const OTHER_HEADERS = [
        'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'accept-encoding' => 'gzip, deflate, br'
    ];

    /** @var Spider */
    private $spider;

    /**
     * @var Parser
     */
    private $parser;
    /** @param EntityManager $entityManager */
    public function __construct($entityManager)
    {
        $this->spider = new Spider(self::OTHER_HEADERS);
        $this->parser = new Parser($entityManager);
    }
    
    public function grab() 
    {
        $pageContent = $this->spider->getPageContent(self::URL_SPIDER);
        $this->parser->parse($pageContent);
    }

}