<?php
namespace App\Controller;

use App\Service\AddStock;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\BonusAction;
use App\Entity\Cafe;

class AddNewStock extends AbstractController
{
    /**
     * @Route("/stock/add", name="stockAdd")
     */
    public function stockAdd(Request $request, AddStock $addStockService)
    {
        $response = new Response();
        $content = $request->getContent();
        if (!empty($content)) {
            $content = json_decode($content, true);
            if($addStockService->add($content)) {
                return $response;
            }
        }
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        return $response;
    }
}