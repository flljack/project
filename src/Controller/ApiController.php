<?php

namespace App\Controller;

use App\Entity\Cafe;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\BonusAction;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/popularBonusAction", name="popularBonusAction")
     */
    public function getPopularBonusAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $popularBonusAction = $entityManager->getRepository(BonusAction::class)->getPopular();
        $json = $this->serialize($popularBonusAction);
        return new Response($json);
    }
    /**
     * @Route("/api/bonusAction/{id}", name="bonusAction", requirements={"id" = "\d+"})
     */
    public function getBonusActionById($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bonusAction = $entityManager->getRepository(BonusAction::class)->getBonusAction($id);
        $json = $this->serialize($bonusAction);
        return new Response($json);
    }

    /**
     * @Route("/api/allCafe", name="allCafe")
     */
    public function getAllCafe()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $cafes = $entityManager->getRepository(Cafe::class)->findAll();
        $json = $this->serialize($cafes);
        return new Response($json);
    }
    /**
     * @Route("/api/allBonusActions", name="allBonusActions")
     */
    public function getAllBonusActions()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bonusActions = $entityManager->getRepository(BonusAction::class)->findAll();
        $json = $this->serialize($bonusActions);
        return new Response($json);
    }

    /**
     * @Route("/api/bonusActionsByCafeIds", name="bonusActionsByCafeIds")
     */
    public function getBonusActionsByCafeIds(Request $request)
    {
        $cafeIds = $request->query->get('cafeIds');
        $page = $request->query->get('page')?:1;
        $userId = (int) $request->query->get('userId');
        if (!empty($cafeIds)) {
            if (!empty($userId)) {
                $this->updateUserFavoritesCafeIds($cafeIds, $userId);
            }
            $cafeIds = explode(',', $cafeIds);
        }
        $entityManager = $this->getDoctrine()->getManager();
        if (empty($cafeIds)) {
            $bonusActions = $entityManager->getRepository(BonusAction::class)->finAllBonusActions($page);
        } else {
            $bonusActions = $entityManager->getRepository(BonusAction::class)->getBonusActionByCafeIds($cafeIds, $page);
        }
        $json = $this->serialize($bonusActions);
        return new Response($json);
    }

    /**
     * @Route("/api/moderateBonusActions", name="moderateBonusActions")
     */
    public function getModerateBonusActions()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bonusActions = $entityManager->getRepository(BonusAction::class)->getAllHiddenBonusActions();
        $json = $this->serialize($bonusActions);
        return new Response($json);
    }

    /**
     * @Route("/api/setModerateBonusAction", name="setModerateBonusAction")
     */
    public function setModerateBonusAction(Request $request)
    {
        $content = $request->getContent();
        $content = json_decode($content, true);
        $bonusActionId = (int) $content['bonusActionId'];
        $cafeId = (int) $content['cafeId'];
        $stockTitle = (string) $content['title'];
        $stockDescription = (string) $content['description'];
        $stockImageUrl = (string) $content['imageUrl'];
        $entityManager = $this->getDoctrine()->getManager();
        $bonusActions = $entityManager->getRepository(BonusAction::class)->find($bonusActionId);
        $cafe = $entityManager->getRepository(Cafe::class)->find($cafeId);
        $bonusActions->setTitle($stockTitle)
            ->setDescription($stockDescription)
            ->setImage($stockImageUrl)
            ->setCafe($cafe)
            ->setHidden(false)
        ;
        $entityManager->flush();
        return new Response();
    }

    /**
     * @Route("/api/removeBonusAction/{id}", name="removeBonusAction", requirements={"id" = "\d+"})
     */
    public function removeBonusAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bonusActions = $entityManager->getRepository(BonusAction::class)->find($id);
        $entityManager->remove($bonusActions);
        $entityManager->flush();
        return new Response();
    }

    /**
     * @Route("/api/userFavoritesCafeIds/{vkId}", name="userFavoritesCafeIds", requirements={"vkId" = "\d+"})
     */
    public  function getUserFavoritesCafeIds($vkId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['vkId' => $vkId]);
        $favoritesCafeIds = '';
        if (empty($user)) {
            $newUser = new User();
            $newUser->setVkId($vkId);
            $entityManager->persist($newUser);
            $entityManager->flush();
        } else {
            $favoritesCafeIds = $user->getFavoritesCafeIds()?:'';
        }
        $json = $this->serialize($favoritesCafeIds);
        return new Response($json);
    }
    /**
     * @Route("/api/isAdmin/{vkId}", name="isAdmin", requirements={"vkId" = "\d+"})
     */
    public function isAdmin($vkId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['vkId' => $vkId]);
        return new Response($this->serialize($user->getModerateAccess()));
    }

    private function updateUserFavoritesCafeIds($cafeIds, $vkId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['vkId' => $vkId]);
        if (!empty($user)) {
            $user->setFavoritesCafeIds($cafeIds);
            $entityManager->flush();
        }
    }

    private function serialize($data)
    {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        return $serializer->serialize($data, 'json');
    }
}
