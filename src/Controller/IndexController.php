<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

final class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Route("/{vueRouting}", name="indexVue")
     * @Route("/stock/{id}", name="bonusActionVue", requirements={"id" = "\d+"})
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('app.html.twig', []);
    }
    
}
