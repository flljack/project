<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="vk_id", type="integer", nullable=false)
     */
    private $vkId;

    /**
     * @var string
     *
     * @ORM\Column(name="favoritesCafeIds", type="string", length=1024, nullable=true)
     */
    private $favoritesCafeIds;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $moderateAccess;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVkId(): ?int
    {
        return $this->vkId;
    }

    public function setVkId(int $vkId): self
    {
        $this->vkId = $vkId;

        return $this;
    }

    public function getFavoritesCafeIds(): ?string
    {
        return $this->favoritesCafeIds;
    }

    public function setFavoritesCafeIds(string $favoritesCafeIds): self
    {
        $this->favoritesCafeIds = $favoritesCafeIds;

        return $this;
    }

    public function getModerateAccess(): ?bool
    {
        return $this->moderateAccess;
    }

    public function setModerateAccess(?bool $moderateAccess): self
    {
        $this->moderateAccess = $moderateAccess;

        return $this;
    }


}
