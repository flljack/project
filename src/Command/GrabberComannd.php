<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class GrabberComannd extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:grab';

    protected function configure()
    {
        $this->addArgument('cafe', InputArgument::REQUIRED, 'What you need?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cafe = $input->getArgument('cafe');
        switch ($cafe) {
            case 'dodo':
                $grabber = $this->getApplication()->getKernel()->getContainer()->get('App\Service\Grabbers\FeedCafes\DodoPizzaRu\Grabber');
                $grabber->grab();
                break;
            case 'milano':
                $grabber = $this->getApplication()->getKernel()->getContainer()->get('App\Service\Grabbers\FeedCafes\Milano12Ru\Grabber');
                $grabber->grab();
                break;
            default:
                echo ('I do not understand what you want' . PHP_EOL);
                break;
        }
    } // choose cafes via switch case
}