<?php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class BonusActionRepository extends EntityRepository
{
    const MAX_BONUS_ACTION_ON_PAGE = 16;
    public function getPopular()
    {
        return $this->createQueryBuilder('ba')
            ->select('ba.title')
            ->addSelect(' ba.id')
            ->addSelect('ba.image')
            ->addSelect('CASE WHEN (ba.url IS NULL) THEN bac.url ELSE ba.url END AS url')
            ->join('ba.cafe', 'bac')
            ->setMaxResults(8)
            ->orderBy('ba.rating', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getBonusAction($id)
    {
        return $this->createQueryBuilder('ba')
            ->select('ba.id, ba.title, ba.description, ba.image, bac.title AS cafeTitle, bac.logo AS cafeLogo')
            ->join('ba.cafe', 'bac')
            ->addSelect('CASE WHEN (ba.url IS NULL) THEN bac.url ELSE ba.url END AS url')
            ->where('ba.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult()
        ;
    }

    public function getBonusActionByCafeIds($cafeIds, $page)
    {
        $offset = ($page - 1 ) * self::MAX_BONUS_ACTION_ON_PAGE;
        return $this->createQueryBuilder('ba')
            ->select('ba')
            ->where('ba.cafe IN (:cafeIds)')
            ->andWhere('ba.hidden = 0')
            ->setParameter('cafeIds', $cafeIds)
            ->setMaxResults(self::MAX_BONUS_ACTION_ON_PAGE)
            ->setFirstResult( $offset )
            ->getQuery()
            ->getResult()
            ;
    }

    public function finAllBonusActions($page)
    {
        $offset = ($page - 1 ) * self::MAX_BONUS_ACTION_ON_PAGE;
        return $this->createQueryBuilder('ba')
            ->select('ba')
            ->andWhere('ba.hidden = 0')
            ->setMaxResults(self::MAX_BONUS_ACTION_ON_PAGE)
            ->setFirstResult( $offset )
            ->getQuery()
            ->getResult()
            ;
    }

    public function getAllHiddenBonusActions()
    {
        return $this->createQueryBuilder('ba')
            ->select('ba.id, ba.title, ba.description, ba.image, bac.title AS cafeTitle, bac.id AS cafeId')
            ->join('ba.cafe', 'bac')
            ->andWhere('ba.hidden = 1')
            ->getQuery()
            ->getResult()
            ;
    }
}