import axios from 'axios';
const urlIsAdmin = '/api/isAdmin/';

var mocks = {
  'auth': { 'POST': { token: 'This-is-a-mocked-token' } },
  'user/me': { 'GET': { name: 'Имя', avatar: 'https://pp.userapi.com/c624728/v624728839/3f8c/dU7Z9TprId0.jpg', id: 0, isAdmin: false} }
}

const apiCall = ({url, method, data}) => new Promise((resolve, reject) => {
  setTimeout(() => {
    try {
      if (url == 'auth') {
        mocks[url][method || 'GET']['token'] = data.id;
      } else if (url == 'user/me') {
        mocks[url][method || 'GET'] = data;
        axios.get(urlIsAdmin + data.id).then(response => {
          mocks[url][method || 'GET']['isAdmin'] = response.data;
          resolve(mocks[url][method || 'GET'])
        });
      }

    } catch (err) {
      reject(new Error(err))
    }
  }, 500)
})

export default apiCall
