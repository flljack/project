import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from '../actions/user'
import apiCall from '../api'
import Vue from 'vue'
import { AUTH_LOGOUT } from '../actions/auth'

const state = { status: '', profile: {} }

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!state.profile.name,
  isAdmin: state => state.profile.isAdmin
}

const actions = {
  [USER_REQUEST]: ({commit, dispatch}, id) => {
    if (id == null) {
      id = localStorage.getItem('user-token');
    }
    VK.Api.call('users.get', {user_ids: id, v:"5.92", fields: "photo_100"}, function(r) {
      if(r.response) {
        let data = {name: r.response[0].first_name, avatar: r.response[0].photo_100, id: r.response[0].id};
        apiCall({url: 'user/me', data: data})
        .then(resp => {
          commit(USER_SUCCESS, resp)
        })
        .catch(resp => {
          commit(USER_ERROR)
          // if resp is unauthorized, logout, to
          dispatch(AUTH_LOGOUT)
        })
        return ;
      }
      commit(USER_ERROR)
      // if resp is unauthorized, logout, to
      dispatch(AUTH_LOGOUT)
      return ;
    });
  },
}


const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, resp) => {
    state.status = 'success';
    Vue.set(state, 'profile', resp)
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
}
