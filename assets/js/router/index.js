import Vue from 'vue'
import Router from 'vue-router'
import Index from '../pages/Index';
import Favorites from '../pages/Favorites';
import Stock from '../pages/Stock';
import AddStock from '../pages/AddStock';
import Store from '../store';
import Moderate from "../pages/Moderate";

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
    if (!Store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/');
  }

const ifAuthenticated = (to, from, next) => {
    if (Store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/auth');
  }

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/favorites',
            name: 'favorites',
            component: Favorites,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/add',
            name: 'AddStock',
            component: AddStock,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/stock/:bonusActionId',
            name: 'stock',
            props: true,
            component: Stock  
        },
        {
            path: '/moderate',
            name: 'moderate',
            component: Moderate,
            beforeEnter: ifAuthenticated
        },
        {
            path: '*', 
            redirect: '/' 
        }
    ]

})
