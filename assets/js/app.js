import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import store from './store/index';
import Vuetify from 'vuetify';
import SocialSharing  from 'vue-social-sharing';
import 'vuetify/dist/vuetify.min.css';
import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);
Vue.use(Vuetify);
Vue.use(SocialSharing);
require('./app.scss');

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
VK.init({
    apiId: 6910704
  });

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: {App},
});

